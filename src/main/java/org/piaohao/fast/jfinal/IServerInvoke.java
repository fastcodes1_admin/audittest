package org.piaohao.fast.jfinal;

import org.apache.catalina.Context;
import org.apache.catalina.startup.Tomcat;

import javax.servlet.ServletException;

public interface IServerInvoke {
    void invoke(Tomcat tomcat, Context currentContext) throws ServletException;
}
