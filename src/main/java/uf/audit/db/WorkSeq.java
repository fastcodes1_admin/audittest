package uf.audit.db;

import java.util.List;
import java.util.UUID;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.plugin.activerecord.DbKit;
import com.jfinal.plugin.activerecord.Model;

import uf.audit.util.Table;

/**
 * 工作序列数据实体
 * 
 * @author sunny
 *
 */
@Table(key = "seqbh")
public class WorkSeq extends Model<WorkSeq> {
	private static final long serialVersionUID = -7644540082576539659L;
	public static WorkSeq dao = new WorkSeq().use(DbKit.MAIN_CONFIG_NAME);

	// 容错处理 如果set的值为空，就不做处理
	public WorkSeq set(String attr, Object value) {
		if (value != null) {
			super.set(attr, value);
		}
		return this;
	}

	// 获取所有序列
	public List<WorkSeq> getSequences() {
		return find("select * from workseq");
	}

	// 更新
	public boolean update(JSONObject obj) {
		String bh = obj.getString("seqbh");
		WorkSeq item = new WorkSeq();
		item.set("seqname", obj.getString("seqname"));
		boolean result = false;
		if (bh != null) {
			item.set("seqbh", obj.getString("seqbh"));
			result = item.update();
		} else {
			item.set("seqbh", UUID.randomUUID().toString());
			result = item.save();
		}
		return result;
	}

	// 删除
	public boolean delete(String bh) {
		return deleteById(bh);
	}
}
