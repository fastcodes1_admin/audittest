package uf.audit.db;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.plugin.activerecord.DbKit;
import com.jfinal.plugin.activerecord.DbPro;
import com.jfinal.plugin.activerecord.IAtom;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Record;

import uf.audit.util.Table;

/**
 * 考试模板数据实体
 * 
 * @author sunny
 *
 */
@Table(key = "modelbh")
public class TestModel extends Model<TestModel> {
	private static final long serialVersionUID = -7644540082576539659L;
	public static TestModel dao = new TestModel().use(DbKit.MAIN_CONFIG_NAME);

	// 容错处理 如果set的值为空，就不做处理
	public TestModel set(String attr, Object value) {
		if (value != null) {
			super.set(attr, value);
		}
		return this;
	}

	// 获取某个年度（所有）的考试计划（计划就是模板）
	public List<TestModel> getplans(String year, String seqbh, String catebh) {
		String sql = "select * from testmodel where 1=1 ";
		if (year != null) {
			sql += " and year=" + year + " ";
		}
		if (seqbh != null) {
			sql += " and seqbh='" + seqbh + "' ";
		}
		if (catebh != null) {
			sql += " and catebh='" + catebh + "' ";
		}
		return find(sql);
	}

	// alias
	public List<TestModel> getmodels(String year, String seqbh, String catebh) {
		return getplans(year, seqbh, catebh);
	}

	// 获取某个计划
	public TestModel getplan(String modelbh) {
		String sql = "select * from testmodel where modelbh=? ";
		return findFirst(sql, modelbh);
	}

	// alias
	public TestModel getmodel(String modelbh) {
		return getplan(modelbh);
	}

	// 获取自己的计划
	public List<Record> getownmodel(String userbh, int type) {
		return DbPro.use().find(
				"select b.* from (select * from authentic where authvalue=? and userbh=?) a left join testmodel b on a.resourcebh=b.modelbh",
				type, userbh);
	}

	// 更新
	public String update(JSONObject obj) {
		String bh = obj.getString("modelbh");
		TestModel item = new TestModel();
		item.set("modelname", obj.getString("modelname"));
		item.set("seqbh", obj.getString("seqbh"));
		item.set("catebh", obj.getString("catebh"));
		item.set("minutes", obj.getInteger("minutes"));
		item.set("year", obj.getInteger("year"));
		item.set("enabled", obj.getInteger("enabled"));
		item.set("remark", obj.getString("remark"));
		if (bh != null) {
			item.set("modelbh", obj.getString("modelbh"));
			if (!item.update()) {
				bh = null;
			}
		} else {
			bh = UUID.randomUUID().toString();
			item.set("modelbh", bh);
			if (!item.save()) {
				bh = null;
			}
		}
		return bh;
	}

	// 删除
	public boolean delete(final String bh) {
		return DbPro.use().tx(new IAtom() {

			public boolean run() throws SQLException {
				DbPro.use().update("delete from marking  where modelbh=?", bh);
				DbPro.use().update("delete from testopt  where testbh in (select testbh from test where modelbh=?)",
						bh);
				DbPro.use().update("delete from testquestion where testbh in (select testbh from test where modelbh=?)",
						bh);
				DbPro.use().update("delete from test where modelbh=?", bh);
				DbPro.use().update("delete from testmodeldetail where modelbh=?", bh);
				DbPro.use().update("delete from testmodel where modelbh=?", bh);
				DbPro.use().update("delete from authentic where resourcebh=?", bh);
				deleteById(bh);
				return true;
			}
		});
	}

	// 克隆
	public boolean clone(final String bh) {
		TestModel m = this.getmodel(bh);
		String nbh = UUID.randomUUID().toString();
		/*
		 * modelname | varchar(50) | YES | | NULL | | | seqbh | varchar(50) |
		 * YES | | NULL | | | catebh | varchar(50) | YES | | NULL | | | minutes
		 * | int(11) | YES | | 60 | | | year | int(11) | YES | | NULL | | |
		 * enabled | int(11) | YES | | 0 | | | remark | varchar(256) | YES | |
		 * NULL | | | builded
		 */
		final TestModel nm = new TestModel();
		nm.set("modelbh", nbh);
		nm.set("modelname", m.getStr("modelname"));
		nm.set("seqbh", m.getStr("seqbh"));
		nm.set("catebh", m.getStr("catebh"));
		nm.set("minutes", m.getInt("minutes"));
		nm.set("year", m.getInt("year"));
		List<TestModelDetail> tmds = TestModelDetail.dao.getmodeldetails(bh);
		final List<TestModelDetail> ntmds = new ArrayList<TestModelDetail>();
		for (TestModelDetail tmd : tmds) {
			TestModelDetail ntmd = new TestModelDetail();
			/*
			 * detailbh | varchar(50) | YES | | NULL | | | modelbh | varchar(50)
			 * | YES | | NULL | | | seqbh | varchar(50) | YES | | NULL | | |
			 * catebh | varchar(50) | YES | | NULL | | | itemtype | varchar(50)
			 * | YES | | NULL | | | itemcount | int(11) | YES | | 10 | | | score
			 * | int(11) | YES | | 1 | | | itemlevel
			 */
			String ntmdbh = UUID.randomUUID().toString();
			ntmd.set("detailbh", ntmdbh);
			ntmd.set("modelbh", nbh);
			ntmd.set("seqbh", tmd.getStr("seqbh"));
			ntmd.set("catebh", tmd.getStr("catebh"));
			ntmd.set("itemtype", tmd.getStr("itemtype"));
			ntmd.set("itemcount", tmd.getInt("itemcount"));
			ntmd.set("score", tmd.getInt("score"));
			ntmd.set("itemlevel", tmd.getInt("itemlevel"));
			ntmds.add(ntmd);
		}
		return DbPro.use().tx(new IAtom() {

			public boolean run() throws SQLException {
				for (TestModelDetail ntmd : ntmds) {
					ntmd.save();
				}
				nm.save();
				return true;
			}
		});
	}
	
	public List<TestModel> gettestscroes(String userbh) {
		String sql = "select a.*,b.modelname from (select * from test where userbh=? and finished=1) a left join testmodel b on a.modelbh=b.modelbh";
		return dao.find(sql, userbh);
	}
}
