package uf.audit.controller;

import java.util.List;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.plugin.activerecord.Record;

import uf.audit.db.Marking;
import uf.audit.db.TestModel;
import uf.audit.db.TestOpt;
import uf.audit.util.Consts;
import uf.audit.util.Controller;
import uf.audit.util.Utils;

/**
 * 阅卷控制器类
 * 
 * @author sunny
 *
 */
@Controller(url = "/yj", view = "/WEB-INF/pages/")
public class MarkingController extends PubController {
	/*
	 * 首页
	 */
	public void index() {
		render("marknav.html");
	}

	/*
	 * 获取我可以批阅的试卷列表
	 */
	public void getmytests() {
		String userbh = Utils.getLogin(this);
		renderJson(TestModel.dao.getownmodel(userbh, Consts.AUTH_MARKING));
	}

	// 当前人员是否是阅卷人员
	public boolean isMarking() {
		return (Boolean) getSessionAttr(Consts.AUTH_MARKING_STR);
	}

	// 获取指定试卷的人员-成绩列表
	public void getscorelist() {
		String modelbh = getPara("modelbh");
		String userbh = getPara("userbh");
		List<Record> markings;
		if (userbh == null)
			markings = Marking.dao.getmodelmarkings(modelbh);
		else
			markings = Marking.dao.getmodelmarkings(modelbh, userbh);
		renderJson(markings);
	}

	// 进入具体阅卷明细页面（个人）
	public void scoredetail() {
		String modelbh = getPara("modelbh");
		String userbh = getPara("userbh");
		Marking.dao.calcScore(modelbh, userbh);
		render("marking.html");
	}

	// 获取主观题的列表
	public void getcustmarkings() {
		String modelbh = getPara("modelbh");
		String userbh = getPara("userbh");
		renderJson(Marking.dao.getcustmarking(modelbh, userbh));
	}

	// 更细试卷分数
	public void postscoredetail() {
		JSONArray arr = JSONArray.parseArray(getPostData());
		boolean result = true;
		for (int i = 0, j = arr.size(); i < j; i++) {
			JSONObject obj = arr.getJSONObject(i);
			result = result && TestOpt.dao.updateSimple(obj);
		}
		if (result)
			renderSuccess();
		else
			renderError();
	}


	// 更新试卷主观题分数
	public void postcustscore() {
		String modelbh = getPara("modelbh");
		String userbh = getPara("userbh");
		int score = getParaToInt("score");
		if (Marking.dao.updateScore(modelbh, userbh, score))
			renderSuccess();
		else
			renderError();
	}
}
