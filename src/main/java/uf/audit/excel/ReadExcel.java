package uf.audit.excel;

import com.alibaba.fastjson.JSON;
import com.jfinal.plugin.activerecord.Record;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import uf.audit.util.Utils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

public class ReadExcel {

    public Map<String, String> readExcel(String filepath) {
        Map<String, String> test = new HashMap<String, String>();//存放题目
        InputStream inputStream;
        int sortnos = 0;
        try {
            //获取文件输入流
            inputStream = Utils.class.getResourceAsStream(filepath);
            //获取excel工作簿
            XSSFWorkbook workbook = new XSSFWorkbook(inputStream);
            //得到Excel工作表对象
            XSSFSheet XSSFSheet = workbook.getSheetAt(0);
            for (Row row : XSSFSheet) {
                List<Record> list = new ArrayList<Record>();//每一道题
                //从第二行开始读取
                if (row.getRowNum() != 0) {
                    row.getCell(0).setCellType(Cell.CELL_TYPE_STRING);
                    Integer sortno = Integer.parseInt(row.getCell(0).getStringCellValue());//题号
                    String itemtype = row.getCell(2).getStringCellValue();//题目类型
                    String itemcontent = row.getCell(4).getStringCellValue();//题目
                    String isanswer = row.getCell(6).getStringCellValue();//答案
                    String opt = row.getCell(5).getStringCellValue();//选项
                    String[] itemopt = opt.split("\n");
                    for (int i = 0; i < 4; i++) {
                        Record item = new Record();//每一道题
                        item.set("sortno", sortno);
                        item.set("itemtype", itemtype);
                        item.set("itembh", sortnos);
                        item.set("itemcontent", itemcontent);
                        item.set("isanswer", isanswer);
                        item.set("optno", itemopt[i].substring(0, 1));
                        item.set("optcontent", itemopt[i].substring(2, itemopt[i].length()));
                        item.set("optbh", UUID.randomUUID().toString());
                        item.set("answer", "0");
                        item.set("score", 10);
                        list.add(item);
                    }
                    sortnos++;
                    String item = JSON.toJSONString(list);
                    test.put("sort" + sortnos, item);
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println(test.size());
            e.printStackTrace();
        } catch (Exception e) {
            System.out.println(test.size());
        }
        return test;
    }

    public static void main(String[] args) {
        ReadExcel re = new ReadExcel();
        Map<String, String> map = re.readExcel("/java.xlsx");
        System.out.println(map.size());
        System.out.println(map.get("sort1"));
    }
}
