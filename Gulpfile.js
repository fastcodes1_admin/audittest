var gulp = require('gulp'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    watch = require('gulp-watch');
 
gulp.task('jsmin', function () {
    return gulp.src(['src/main/webapp/assets/js/o/test23596.js', 'src/main/webapp/assets/js/o/aesutil.js', 'src/main/webapp/assets/js/o/angular.min.js'])
        .pipe(uglify({mangle: true, compress: true,}))
        .pipe(gulp.dest('src/main/webapp/assets/js/'));
});

gulp.task('concat', ['jsmin'], function() {
  return gulp.src(['src/main/webapp/assets/js/angular.min.js', 'src/main/webapp/assets/js/o/aes.js', 'src/main/webapp/assets/js/o/pbkdf2.js', 'src/main/webapp/assets/js/aesutil.js', 'src/main/webapp/assets/js/test23596.js'])
    .pipe(concat({ path: 'test.js', newLine: ';'}))
    .pipe(gulp.dest('src/main/webapp/assets/js/'));
});

gulp.task('watch', function(){
    return watch('src/main/webapp/assets/js/o/test23596.js', function () {
        gulp.start('concat');
    })});

gulp.task('default', ['jsmin', 'concat', 'watch']);

